import sys
import getopt
from seq2seq_model.dataset_generator.dataset_generator import DatasetGenerator
from seq2seq_model.dataset_generator.placeholder_interpolator import PlaceholderInterpolator


def main(argv):
    labels_folder = ''
    templates_folder = ''
    dataset_folder = ''
    n_records = 1000
    print("Starting dataset generation")

    try:
        opts, args = getopt.getopt(argv, "hl:t:d:n:", ["labels=", "templates=", "dataset=", "nrecord="])
    except getopt.GetoptError:
        print('main.py -l <labels_folder> -t <templates_folder> -d <dataset_folder> -n <record_number>')
        sys.exit(2)
    if len(argv) == 0:
        print('main.py -l <labels_folder> -t <templates_folder> -d <dataset_folder> -n <record_number>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('main.py -l <labels_folder> -t <templates_folder> -d <dataset_folder> -n <record_number>')
            sys.exit()
        elif opt in ("-l", "--labels"):
            labels_folder = arg
        elif opt in ("-t", "--templates"):
            templates_folder = arg
        elif opt in ("-d", "--dataset"):
            dataset_folder = arg
        elif opt in ("-n", "--nrecord"):
            n_records = int(arg)

    question_templates_file_path = f"{templates_folder}/question-templates.json"
    sparql_templates_file_path = f"{templates_folder}/sparql-templates.json"

    paths = {
        "labels": labels_folder,
        "questions": question_templates_file_path,
        "sparql": sparql_templates_file_path
    }
    pi = PlaceholderInterpolator()
    generator = DatasetGenerator(interpolator=pi, paths=paths)
    labels = generator.extract_labels()
    templates = generator.extract_templates()
    template_labels = generator.extract_placeholders(templates=templates, labels=labels)
    generator.generate_dataset(
        template_labels=template_labels,
        output_dir_name=dataset_folder,
        filename="generated-dataset.json",
        n_records=n_records)
    print("Dataset generated")


if __name__ == "__main__":
    main(sys.argv[1:])
