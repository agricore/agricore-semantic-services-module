import re
from random import choice


class PlaceholderInterpolator:

    def interpolate(self, template: str, label: dict) -> str:
        """
        Given a template string and a label dictionary mapping each placeholder to
        a pre-determined random value, replace the placeholder occurrences in the
        template with the random value.
        """
        pattern = re.compile("|".join(label.keys()))
        q = pattern.sub(lambda m: label[re.escape(m.group(0))], template)
        return q

    def get_random_label(self, labels: dict) -> dict:
        """
        Given a labels dictionary mapping each label name to the possible values
        it can take, returns a dictionary with the same keys but only one value
        for each key, which will be a randomly chosen value from the list. If the
        label is the start_date, the end_date is the one with the same index in the
        list of end date values (this is to get the right correspondence when
        performing the query).
        """
        random_values = {}
        for k, values in labels.items():
            if k == '<end_date>':
                continue
            elif k == '<start_date>':
                idx = choice(list(range(len(values))))
                random_values[k] = values[idx]
                random_values['<end_date>'] = labels['<end_date>'][idx]
            else:
                random_values[k] = choice(labels[k])
        return random_values
