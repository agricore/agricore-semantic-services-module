import json
import os
import re
from pathlib import Path

from seq2seq_model.dataset_generator.placeholder_interpolator import PlaceholderInterpolator


class DatasetGenerator:
    interpolator: PlaceholderInterpolator

    def __init__(self, interpolator: PlaceholderInterpolator, paths: dict) -> None:
        self.interpolator = interpolator
        self.paths = paths

    def build_json_question_sparql(self, sparql: str, questions: list) -> list:
        return [{"question": question, "sparql": sparql} for question in questions]

    def create_result_dir_name(self, result_dir_name: str) -> Path:
        """
        Creates the directory where to store the datasets,
        giving it a name of result_dir_name.
        """
        output_dir = Path(result_dir_name)
        output_dir.mkdir(parents=True, exist_ok=True)
        return output_dir

    def store_dataset_records(self, result_dir_name: str, filename: str, results: list) -> None:
        """
        Store the newly created record in a json file compliant with the format HuggingFace's
        APIs want to generate a dataset.

        :param result_dir_name: folder name for the results
        :param filename: filename complete file extension
        :param results: list of query results
        """
        output_dir = self.create_result_dir_name(result_dir_name)

        with open(Path(output_dir).joinpath(filename), "a+") as f_records:
            for result in results:
                f_records.write(json.dumps(result) + '\n')

        f_records.close()

    def extract_start_end_dates(self, label_dict: dict) -> dict:
        """
        Given a dictionary of labels, extracts the 'dataset_data_range' values and splits
        start dates from end dates. To work correctly, the starting date must be the
        first value in the list, while the ending date must be the last value in the list.
        """
        label_dict['start_date'] = [x.split(',')[0] for x in label_dict['dataset_date_range']]
        label_dict['end_date'] = [x.split(',')[1] for x in label_dict['dataset_date_range']]
        return label_dict

    def get_label_name(self, placeholder: str):
        """
        Given a placeholder, returns the correct label name to be replaced in the
        SPARQL queries and in the question templates.
        """
        if placeholder == "country_name":
            return "country"
        elif placeholder == "continent_name":
            return "continent"
        elif placeholder == "analysis_unit_ref":
            return "analysis_unit_reference"
        elif placeholder == "variable_name_or_value":
            return "variable"
        elif placeholder == "start_date":
            return "start_date"
        else:
            return "end_date"

    def extract_labels(self) -> dict:
        """
        Reads the label files and returns a dictionary mapping each filename (without
        the .txt extension) to the list contained into the file itself. The files must
        contain a label for each line, and the filename must be exactly the name of the
        label (continent, country, dataset_theme, etc.).
        """
        labels_path = self.paths["labels"]
        label_files = os.listdir(labels_path)
        label_dict = {}
        for file in label_files:
            with open(os.path.join(labels_path, file)) as f:
                labels = [line.strip().lower() for line in f]
                label_dict[file.replace(".txt", "")] = labels
        label_dict = self.extract_start_end_dates(label_dict)
        return label_dict

    def extract_templates(self) -> dict:
        """
        Reads the file containing the questions template and the file containing the SPARQL
        templates, filters the questions (with date or without date) and returns a dictionary
        mapping each type of query (dataset theme, country coverage, etc.) to each query and
        list of questions related to that specific type.
        """
        question_file = self.paths["questions"]
        sparql_file = self.paths["sparql"]
        with open(question_file) as f:
            questions = json.load(f)
        with open(sparql_file) as f:
            queries = json.load(f)
        return {query["type"]: self.filter_questions(query, questions) for query in queries}

    def filter_questions(self, query: dict, questions: dict) -> dict:
        """
        Given a SPARQL query and list of questions, splits the questions containing a date parameter
        from the questions not containing it, and returns a dictionary with those filtered questions
        along with the original SPARQL query.
        """
        query_id = query["id"]
        filtered_questions = [question["template"] for question in questions if question["sparql-template"] == query_id]
        filtered_questions_with_date = list(filter(lambda q: q.find('<start_date>') > 0, filtered_questions))
        filtered_questions_no_date = list(set(filtered_questions) - set(filtered_questions_with_date))
        return {"sparql": query["template"], "questions_with_date": filtered_questions_with_date,
                "questions_no_date": filtered_questions_no_date}

    def extract_placeholders(self, templates: dict, labels: dict):
        """
        Given a dictionary of templates (SPARQL and NLP questions) and list of possible labels,
        searches all the placeholders in the templates (namely, the one between angle brackets,
        like <dataset_theme>) and creates a dictionary mapping each placeholder to all the
        possible labels it can be replaced with. Said dictionary is then saved into the `templates`
        dictionary on the `labels` key.
        """
        for _, template in templates.items():
            template["labels"] = {}
            sparql = template["sparql"]
            found = re.findall(r'<\w*>', sparql)
            placeholders = list(map(lambda x: x[1:-1], set(found)))
            for placeholder in placeholders:
                template["labels"][placeholder] = labels[placeholder] \
                    if placeholder in labels \
                    else labels[self.get_label_name(placeholder)]
        return templates

    def link_interpolated_questions_sparql(self, sparql: str, questions: list, random_label: dict) -> list:
        """
        Given a SPARQL query, a list of questions and a label dictionary containing the
        mapping between a placeholder and a random value to be replaced with, performs
        the interpolation on both query and questions, and then creates a list of JSON
        values formatted in this way:

                {"question": question, "sparql": sparql}

        """
        sparql_result = self.interpolator.interpolate(sparql, random_label)
        question_results = [self.interpolator.interpolate(question, random_label) for question in questions]
        return self.build_json_question_sparql(sparql_result, question_results)

    def generate_and_store_dataset_record(self, template_labels: dict, output_dir_name: str, filename: str) -> None:
        """
        Given a dictionary containing a SPARQL query, a list of related questions and
        a list of labels that can be replaced into said query and questions, uses the
        interpolator object to do the replacement process for both question with dates
        and question without dates, merges them into a single list and saves them into
        a JSON file.
        """
        for _, value in template_labels.items():
            sparql = value["sparql"]
            questions_with_date = value['questions_with_date']
            questions_no_date = value['questions_no_date']
            labels = value["labels"]
            labels = {f'<{k}>': v for k, v in labels.items()}

            random_label = self.interpolator.get_random_label(labels)
            records_with_date = self.link_interpolated_questions_sparql(sparql, questions_with_date, random_label)

            random_label['<start_date>'] = random_label['<end_date>'] = "''"
            records_no_date = self.link_interpolated_questions_sparql(sparql, questions_no_date, random_label)

            value_records = records_with_date + records_no_date
            self.store_dataset_records(output_dir_name, filename, value_records)

    def generate_dataset(self, template_labels: dict, output_dir_name: str,
                         filename: str, n_records: int = 100) -> None:
        """
        Generates n_records records and clusters them into a single dataset file, using the
        generate_and_store_dataset_record method for each record. 
        """
        [self.generate_and_store_dataset_record(template_labels, output_dir_name, filename) for _ in range(n_records)]
