# Dataset Generator

The dataset generation consists in two main Python classes that have to work
together to build a correctly defined dataset. The resulting dataset will then
be given as input to the Deep Learning model, so that it can learn how to
predict SPARQL queries given a question in natural language format.

Both classes come with related test that can be used to check if the
classes perform in the correct way. 

### DatasetGenerator class
The DatasetGenerator class contains various methods useful to load templates
(regarding on both SPARQL and NL questions), extract the placeholders from them,
load the labels and the possible values they can take and then interpolate the
placeholders with random label values.

The templates have to a well-defined structure to make the class correctly behave
and generate the dataset without failure. The question template's structure has to be
this one:

```json
[
  {
    "id": 1,
    "type": "dataset theme",
    "sparql-template": 2,
    "template": "Find all datasets that cover the theme <dataset_theme>"
  },
  {
    "id": 2,
    "type": "country coverage",
    "sparql-template": 4,
    "template": "Which are the datasets with geographical coverage <country_name> and time span <start_date> - <end_date>?"
  }
]
```

while the SPARQL template's structure must the following:

```json
[
  {
    "id": 1,
    "type": "dataset theme",
    "template": "select distinct ?dataset where { ?dataset a agricore:Dataset . ?dataset dcat:theme ?theme . FILTER ( ?theme IN (?subject)) . { SERVICE SPARQL_ENDPOINT { SELECT  ?subject WHERE { graph EU_AUTH_THEME_GRAPH { ?subject  ?predicate  ?object . }  ?object a skos:Concept . ?subject skos:prefLabel ?label . FILTER ( regex(?label, '<dataset_theme>'@en, 'i') ) . } } } union { select ?subject where { graph EU_INSPIRE_THEME_GRAPH { ?subject  ?predicate  ?object . } ?object a prov:DataItem . ?object terms:title ?title . FILTER ( lang(?title) = 'en') FILTER ( regex(?title, '<dataset_theme>', 'i') ) . } } BIND (IF(STRLEN(str(<start_date>)) > 0, <start_date>, 'NaN') AS ?startParam) BIND (IF(STRLEN(str(<end_date>)) > 0, <end_date>, 'NaN') AS ?endParam) OPTIONAL { ?dataset terms:temporal ?dateRange . ?dateRange a terms:PeriodOfTime . ?dateRange dcat:startDate ?startDate . ?dateRange dcat:endDate ?endDate . BIND (IF(?startParam <= year(?startDate), year(?startDate), ?startParam) AS ?max_start_date) BIND (IF(?endParam <= year(?endDate), ?endParam, year(?endDate)) AS ?min_end_date) FILTER (isNumeric(?startParam) && ?max_start_date <= year(?endDate) && isNumeric(?endParam) && ?min_end_date >= year(?startDate)) . } FILTER ((bound(?startDate) || ?startParam ='NaN') && (bound(?endDate) || ?endParam = 'NaN')) }"
  },
  {
    "id": 2,
    "type": "country coverage",
    "template": "select distinct ?dataset where { ?dataset a agricore:Dataset . ?dataset agricore:hasGeoCoverage ?geozone . FILTER ( ?geozone IN (?country_subject)) . SERVICE SPARQL_ENDPOINT { SELECT  ?country_subject WHERE { graph COUNTRY_GRAPH { ?country_subject  ?country_predicate  skos:Concept . } ?country_subject skos:prefLabel ?country_name . FILTER ( lang(?country_name) = 'en') FILTER ( regex(?country_name, '<country_name>'@en, 'i')) } } BIND (IF(STRLEN(str(<start_date>)) > 0, <start_date>, 'NaN') AS ?startParam) BIND (IF(STRLEN(str(<end_date>)) > 0, <end_date>, 'NaN') AS ?endParam) OPTIONAL { ?dataset terms:temporal ?dateRange . ?dateRange a terms:PeriodOfTime . ?dateRange dcat:startDate ?startDate . ?dateRange dcat:endDate ?endDate . BIND (IF(?startParam <= year(?startDate), year(?startDate), ?startParam) AS ?max_start_date) BIND (IF(?endParam <= year(?endDate), ?endParam, year(?endDate)) AS ?min_end_date) FILTER (isNumeric(?startParam) && ?max_start_date <= year(?endDate) && isNumeric(?endParam) && ?min_end_date >= year(?startDate)) . } FILTER ((bound(?startDate) || ?startParam ='NaN') && (bound(?endDate) || ?endParam = 'NaN')) }"
  }
]
```

The `sparql_template` key in each entry of the question template must have
a value corresponding to the `id` value in the SPARQL template; this way, the
question is related the SPARQL query.

It can be noticed that both questions and queries have, in the templates, words
wrapped between angular brackets; those are the _placeholders_
that the DatasetGenerator, paired with the PlaceholderInterpolator, substitute
with correct label values. Therefore, it is mandatory that those placeholders
are always defined like that.

The label files too must have pre-defined structure to be correctly read by the
DatasetGenerator class. Indeed, each file must have a filename equal to the label
name, while the content of the file has to be one label value for each line. Also,
the file has to be a `.txt` file.  
To make an example, we can have a file `dataset_theme.txt` which content can be
structured in this way:

```text
Agriculture, fisheries, forestry and food
Economy and finance
Land cover
Land use
Orthoimagery
Education, culture and sport
Energy
Coordinate reference systems
Environment
```

While the dataset files have to be created manually, the label files can be
generated using the **labels_extraction** script in this repository.

### PlaceholderInterpolator
This class can be used to pick random values from a dictionary of possible label
values. It can also be used to interpolate a list of values into a set
of templates containing placeholders that can be mapped by name to the
before-mentioned values.  
The class can be coupled with an instance of DatasetGenerator to create a
dataset to be fed into the Machine Learning model.

### How To

The `main` function in the _main.py_ script has been adopted to create the dataset,
defining an instance of both DatasetGenerator and PlaceholderInterpolator and using
them to originate a list of values to be put into a single file.
The script has to run from a terminal with the following command from the project root:

```shell
python -m seq2seq_model.dataset_generator.main -l <labels_folder> -t <templates_folder> -d <destination_folder> -n <number_of_records>
```

The parameters are the following:
- `-l` that describes the path of the folder in which the labels are stored;
- `-t` that describes the path of the folder in which the templates are stored;
- `-d` that describes the path in which the dataset will be stored in the file system;
- `-n` that specifies the number of times the records will be replicated in the dataset.

Here's a simple example to use this command:

```shell
python -m seq2seq_model.dataset_generator.main -l ./labels -t ./seq2seq_model/templates -d ./output-folder -n 10
```

#### NOTE
The question and sparql templates must be a single file for each of them,
therefore it is also necessary to change the following lines and setting
`<question_filename>` and `<sparql_filename>` to the `.json` file containing the templates.

```python
question_templates_file_path = f"{templates_path}/<question_filename>.json"
sparql_templates_file_path = f"{templates_path}/<sparql_filename>.json"
```

