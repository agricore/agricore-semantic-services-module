## Seq2Seq Model

To correctly transform a user question in natural language into a corresponding
SPARQL query (which is then used to retrieve the datasets and their metadata
from GraphDB) a Seq2Seq model has been trained, following the tutorials available in
[HuggingFace](https://huggingface.co/).

The model generation followed this steps:
- first, it has been created a dataset to fed to the model. The dataset maps natural language
questions to a SPARQL queries, and it has been defined through a set of templates (both for
questions and SPARQL queries) that are then interpolated with specific Agricore labels.
In this way, it has been possible to generate a large dataset of over 80.000
records.
- when the dataset was ready, it's been used to train the model. The training was performed
through a Jupyter Notebook (using Google Cloud services to speed up the process), and the model
was then uploaded to the HuggingFace repository to be easily used in a later time.

The model is then put in use in the search pipeline of the haystack semantic service. You can read
about it in the _semantic_services_ folder README.

### Folder Structure

Following, the content of the folders in this directory.
- The `dataset_generator` folder contains some Python classes that have been used to generate
the model input dataset. You can read about how they work in the README contained in said folder;
- The `jupyter_notebooks` folder contains the two notebooks used to train the model and the
custom tokenizer (a particular component used in Deep Learning to encode and decode words);
- The `labels_extraction` folder contains the Python scripts used to extract the labels from
already-present datasets in a GraphDB instance;
- The `templates` folder contains the templates that have been adopted to create the dataset;
- The `tests` folder contains various Python test regarding the dataset generation and the
label interpolation described above. These tests ensure that the classes behave correctly.
