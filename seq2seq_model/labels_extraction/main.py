#!/usr/bin/python
import getopt
import sys
from pathlib import Path
from typing import Any, Dict, List

from haystack.document_stores import GraphDBKnowledgeGraph

from seq2seq_model.labels_extraction.query_configuration import queries, prefixes


def connect_to_graphdb(config: Dict) -> Any:
    """
    Connect to an active GraphDB instance as requested in the config

    :param config: configuration dictionary containing the connection credentials to the GraphDB repository
    """
    kg = GraphDBKnowledgeGraph(
        host=config['graphdbHost'],
        port=config['graphdbPort'],
        index=config['graphdbRepo']
    )

    print(f"The last triple stored in the knowledge graph is: {kg.get_all_triples()[-1]}")
    print(f"There are {len(kg.get_all_triples())} triples stored in the knowledge graph.")

    kg.prefixes = prefixes
    return kg


def create_result_dir_name(result_dir_name: str) -> Path:
    """
    Create a directory where the labels will be saved after the extraction process,
    if it doesn't already exist.

    :param result_dir_name: the name of the folder that has to be created
    """
    print("check if directory for labels does not exist...", flush=True)
    output_dir = Path(result_dir_name)
    output_dir.mkdir(parents=True, exist_ok=True)
    return output_dir


def store_results_txt(result_dir_name: str, filename: str, results: list) -> None:
    """
    Store the results of the query in a txt file called filename.

    :param result_dir_name: folder name for the results
    :param filename: filename complete file extension
    :param results: list of query results
    """
    output_dir = create_result_dir_name(result_dir_name)

    with open(Path(output_dir).joinpath(filename + '.txt'), "w+") as f_labels:
        for result in results:
            f_labels.write(result + '\n')

    f_labels.close()


def query_kg(knowledge_graph: GraphDBKnowledgeGraph, sparql_query: str) -> List[str]:
    """
    Execute a single SPARQL query on the knowledge graph to retrieve an answer and unpack
    different answer styles for boolean queries, count queries, and list queries.

    :param knowledge_graph: an instance of the Haystack GraphDbKnowledgeGraph already initialized
    :param sparql_query: SPARQL query that shall be executed on the knowledge graph
    """
    try:
        response = knowledge_graph.query(sparql_query=sparql_query)

        # unpack different answer styles
        if isinstance(response, list):
            if len(response) == 0:
                result = ""
            else:
                result = []
                for x in response:
                    for k, v in x.items():
                        result.append(v["value"])
        elif isinstance(response, bool):
            result = str(response)
        elif "count" in response[0]:
            result = str(int(response[0]["count"]["value"]))
        else:
            result = ""

    except Exception as e:
        print(e)
        result = ""

    return result


def main(argv):
    graphdb_host = 'localhost'
    graphdb_port = 8200
    graphdb_repo = 'ardit'
    labels_folder = 'labels'

    try:
        opts, args = getopt.getopt(argv, "hg:p:r:l:", ["host=", "port=", "repo=", "labels="])
    except getopt.GetoptError:
        print('main.py -g <graphdb_host> -p <graphdb_port> -r <graphdb_repo> -l <labels_folder>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('main.py -g <graphdb_host> -p <graphdb_port> -r <graphdb_repo> -l <labels_folder>')
            sys.exit()
        elif opt in ("-g", "--graphdbhost"):
            graphdb_host = arg
        elif opt in ("-p", "--graphdbport"):
            graphdb_port = int(arg)
        elif opt in ("-r", "--graphdbrepo"):
            graphdb_repo = arg
        elif opt in ("-l", "--labels"):
            labels_folder = arg

    config = {
        "graphdbHost": graphdb_host,
        "graphdbPort": graphdb_port,
        "graphdbRepo": graphdb_repo,
        "resultDirName": labels_folder
    }
    print(f"Connecting to Agricore Knowledge Graph in GraphDB repository {config['graphdbRepo']} ...")

    kg = connect_to_graphdb(config)

    for key, sq in queries.items():
        print("Executing the SPARQL query for " + key)
        results = query_kg(knowledge_graph=kg, sparql_query=sq)
        print(results)

        store_results_txt(config['resultDirName'], key, results)


if __name__ == "__main__":
    main(sys.argv[1:])
