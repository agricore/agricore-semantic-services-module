queries = {
    'dataset_purpose': '''
        select distinct ?label 
        where {
            graph <https://agricore-project.eu/ontology/agricore-dcatap/DatasetPurpose> 
            {
                ?subject ?predicate  ?type .
            } ?type a skos:Concept .
            ?type skos:prefLabel ?label .
            FILTER ( lang(?label) = "en") .
        }
    ''',
    'dataset_theme': '''
        select distinct ?label
        where {
            {
                SERVICE <http://publications.europa.eu/webapi/rdf/sparql>
                {
                    SELECT  ?label
                    WHERE
                    {
                        graph <http://publications.europa.eu/resource/authority/data-theme> {
                            ?subject  ?predicate  ?object .
                        } 
                        ?object a skos:Concept .
                        ?subject skos:prefLabel ?label .
                        FILTER ( lang(?label) = "en") .
                    }
                }
            } union
            {
                select ?label
                where 
                {
                    graph <http://inspire.ec.europa.eu/theme> {
                        ?subject  ?predicate  ?object .
                    } 
                    ?object a <http://purl.org/net/provenance/ns#DataItem> .
                    ?object <http://purl.org/dc/terms/title> ?label .
                    FILTER ( lang(?label) = "en") .
                }
            }
        }
''',
    'dataset_type': '''
        select ?label 
        where {
            graph <https://agricore-project.eu/ontology/agricore-dcatap/subjects> {
                ?subject ?predicate  ?type .
            } ?type a skos:Concept .
            ?type skos:prefLabel ?label .
            filter ( lang(?label) = "en") .
        }
    ''',
    'country': '''
       select distinct ?country_name 
       where {
            service <http://publications.europa.eu/webapi/rdf/sparql> 
            {
                select ?country_name where {
                    graph <http://publications.europa.eu/resource/authority/country> 
                    {
                        ?country_subject  ?country_predicate  skos:Concept .
                    } ?country_subject skos:prefLabel ?country_name .
                    FILTER ( lang(?country_name) = "en") .
                } 
            } 
       }
    ''',
    'continent': '''
       select distinct ?continent_name 
       where {
            service <http://publications.europa.eu/webapi/rdf/sparql> 
            {
                select ?continent_name where {
                    graph <http://publications.europa.eu/resource/authority/continent> 
                    {
                        ?cont_subject  ?cont_predicate skos:Concept .
                    } ?cont_subject skos:prefLabel ?continent_name .
                    FILTER ( lang(?continent_name) = "en") .
                }
            }
       } 
    ''',
    'analysis_unit_reference': '''
        select distinct ?label 
        where {
            graph <https://agricore-project.eu/ontology/agricore-dcatap/AnalysisUnitReference>
            {
                ?unit ?predicate ?object .
            } 
            ?unit a agricore:AnalysisUnitReference .
            ?unit skos:prefLabel ?label .
        }
    ''',
    'variable': '''
    select distinct ?variableRefLabel 
    where {
        {
            select ?variableRefLabel where {
                ?dataset a agricore:Dataset .
                ?dataset agricore:hasDatasetVariables ?datasetVariable .
                ?datasetVariable agricore:variableName ?variableRefLabel .
            } 
        } 
        union {
            select ?variableRefLabel where {
                ?dataset a agricore:Dataset .
                ?dataset agricore:hasDatasetVariables ?datasetVariable .
                ?datasetVariable agricore:referenceValues ?variableRefLabel .
            } 
        } 
    }
    ''',
    'dataset_date_range': 'select ?res where { ' +
                          ' BIND(concat(str(year(?startDate)), ",", str(year(?endDate)) ) as ?res)' +
                          ' ?dataset a agricore:Dataset .' +
                          ' ?dataset terms:temporal ?dateRange .   ' +
                          ' ?dateRange a terms:PeriodOfTime .      ' +
                          ' ?dateRange dcat:startDate ?startDate . ' +
                          ' ?dateRange dcat:endDate ?endDate       ' +
                          ' }'
}

prefixes = """PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX agricore: <https://agricore-project.eu/ontology/agricore-dcatap#>
    PREFIX dcat: <http://www.w3.org/ns/dcat#>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    PREFIX terms: <http://purl.org/dc/terms/>
    PREFIX prov: <http://purl.org/net/provenance/ns#>
    """


