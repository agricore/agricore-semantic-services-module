# Extract labels from ARDIT vocabularies
Given the public and private vocabularies from ARDIT related to

- theme
- purpose
- geo coverage
- subjects
- analysis unit

and in the datasets chatacterized within Ardit

- dataset variables
- temporal extent (start_date and end_date)

the python scripts contained in this folder allow to extract the labels corresponding to the URI and store them in text files. 

### Setup 

In order to execute the python scripts Haystack is needed. 

For installation guidelines of Haystack globally or in a virtual environment we refer to [this README](https://gitlab.com/stamtech/agricore/-/blob/29-use-pretrained-t5-model-with-haystack/haystack-semantic-services/README.md) 

### Running the scripts 

Before running the script, please make sure to have followed the instructions in the Setup section. 
Moreover, make sure to have a running instance of GraphDB available with the Agricore Knowledge Graph successfully built. 

The script can be run with the following command:

```shell
python -m seq2seq_model.labels_extraction.main
```

You can also specify a couple of parameters to change the GraphDB
configuration and also where the labels have to be stored in the file system.  
The parameters are:
- `-g` to specify the GraphDB host (defaults to _localhost_);
- `-p` to specify the GraphDB port (defaults to _8200_);
- `-r` to specify the GraphDB repository (defaults to _ardit_);
- `-l` to specify the folder in which the labels have to be stored. Defaults to a _label_ directory
in the root folder.

A simple example where you specify a different port and a different output folder:

```bash
python -m seq2seq_model.labels_extraction.main -p 7200 -l .\seq2seq_model\labels
```

The labels will be extracted in text files in the specified folder.  
The filename of each file corresponds to the concept for which the labels have been extracted.   
For instance the file `analysis_unit_reference.txt` contains the labels of analysis unit references of the datasets characterized in ARDIT. 
