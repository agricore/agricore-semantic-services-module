import unittest
from pathlib import Path
from seq2seq_model.dataset_generator.dataset_generator import DatasetGenerator
from seq2seq_model.dataset_generator.placeholder_interpolator import PlaceholderInterpolator


class TestDatasetGenerator(unittest.TestCase):

    def test_extract_labels(self):
        labels_path = Path(__file__).parent / "samples/labels"
        paths = {
            "labels": labels_path
        }
        pi = PlaceholderInterpolator()
        generator = DatasetGenerator(pi, paths)
        labels = generator.extract_labels()
        self.assertEqual(['energy', 'environment', 'health'], labels.get('dataset_theme'))
        self.assertEqual(['2006', '2009'], labels.get('start_date'))
        self.assertEqual(['2018', '2009'], labels.get('end_date'))

    def test_extract_template(self):
        templates_path = Path(__file__).parent.parent / "templates"
        question_templates_file_path = f"{templates_path}/question-templates.json"
        sparql_templates_file_path = f"{templates_path}/sparql-templates.json"
        paths = {
            "questions": question_templates_file_path,
            "sparql": sparql_templates_file_path
        }
        pi = PlaceholderInterpolator()
        generator = DatasetGenerator(pi, paths)
        templates = generator.extract_templates()
        self.assertEqual(len(templates.values()), 10)
        self.assertIn("dataset theme", templates)
        self.assertIn('<dataset_theme>', templates['dataset theme']['sparql'])

    def test_link_interpolated_questions_sparql(self):
        paths = {}
        pi = PlaceholderInterpolator()
        generator = DatasetGenerator(pi, paths)
        labels = {
            '<dataset_theme>': ["land cover"],
            '<start_date>': ["''"],
            '<end_date>': ["''"]
        }
        random_label = pi.get_random_label(labels)
        print(random_label)
        sparql = "select distinct ?dataset ?startDate ?endDate where { ?dataset a agricore:Dataset . ?dataset " \
                 "dcat:theme ?theme . FILTER ( ?theme IN (?subject)) . { SERVICE " \
                 "<http://publications.europa.eu/webapi/rdf/sparql> { SELECT  ?subject WHERE { graph " \
                 "<http://publications.europa.eu/resource/authority/data-theme> { ?subject  ?predicate  ?object . }  " \
                 "?object a skos:Concept . ?subject skos:prefLabel ?label . FILTER ( regex(?label, " \
                 "'<dataset_theme>'@en, 'i') ) . } } } union { select ?subject where { graph " \
                 "<http://inspire.ec.europa.eu/theme> { ?subject  ?predicate  ?object . } ?object a " \
                 "<http://purl.org/net/provenance/ns#DataItem> . ?object <http://purl.org/dc/terms/title> ?title . " \
                 "FILTER ( lang(?title) = 'en') FILTER ( regex(?title, '<dataset_theme>', 'i') ) . } } BIND (IF(" \
                 "STRLEN(str(<start_date>)) > 0, <start_date>, 'NaN') AS ?startParam) BIND (IF(STRLEN(str(" \
                 "<end_date>)) > 0, <end_date>, 'NaN') AS ?endParam) OPTIONAL { ?dataset terms:temporal ?dateRange . " \
                 "?dateRange a terms:PeriodOfTime . ?dateRange dcat:startDate ?startDate . ?dateRange dcat:endDate " \
                 "?endDate . BIND (IF(?startParam <= year(?startDate), year(?startDate), ?startParam) AS " \
                 "?max_start_date) BIND (IF(?endParam <= year(?endDate), ?endParam, year(?endDate)) AS ?min_end_date) " \
                 "FILTER (isNumeric(?startParam) && ?max_start_date <= year(?endDate) && isNumeric(?endParam) && " \
                 "?min_end_date >= year(?startDate)) . } FILTER ((bound(?startDate) || ?startParam ='NaN') && (bound(" \
                 "?endDate) || ?endParam = 'NaN')) } "
        questions = ["Find all datasets that cover the theme <dataset_theme>",
                     "Which are the datasets that cover the theme <dataset_theme>?"]
        result = generator.link_interpolated_questions_sparql(sparql, questions, random_label)

        self.assertTrue(len(result) == 2)

        interpolated_sparql = result[0]["sparql"]
        self.assertTrue(interpolated_sparql.find("land cover") > 0)

        for x in result:
            self.assertTrue(x["question"].find("land cover") > 0)


if __name__ == '__main__':
    unittest.main()
