import unittest

from seq2seq_model.dataset_generator.placeholder_interpolator import PlaceholderInterpolator


class TestPlaceholderInterpolator(unittest.TestCase):

    def test_label_interpolator(self):
        label = {
            '<dataset_theme>': "land cover",
            '<start_date>': "''",
            '<end_date>': "''"
        }
        template = "select distinct ?dataset ?startDate ?endDate where { ?dataset a agricore:Dataset . ?dataset " \
                   "dcat:theme ?theme . FILTER ( ?theme IN (?subject)) . { SERVICE " \
                   "<http://publications.europa.eu/webapi/rdf/sparql> { SELECT  ?subject WHERE { graph " \
                   "<http://publications.europa.eu/resource/authority/data-theme> { ?subject  ?predicate  ?object . } " \
                   " ?object a skos:Concept . ?subject skos:prefLabel ?label . FILTER ( regex(?label, " \
                   "'<dataset_theme>'@en, 'i') ) . } } } union { select ?subject where { graph " \
                   "<http://inspire.ec.europa.eu/theme> { ?subject  ?predicate  ?object . } ?object a " \
                   "<http://purl.org/net/provenance/ns#DataItem> . ?object <http://purl.org/dc/terms/title> ?title . " \
                   "FILTER ( lang(?title) = 'en') FILTER ( regex(?title, '<dataset_theme>', 'i') ) . } } BIND (IF(" \
                   "STRLEN(str(<start_date>)) > 0, <start_date>, 'NaN') AS ?startParam) BIND (IF(STRLEN(str(" \
                   "<end_date>)) > 0, <end_date>, 'NaN') AS ?endParam) OPTIONAL { ?dataset terms:temporal ?dateRange " \
                   ". ?dateRange a terms:PeriodOfTime . ?dateRange dcat:startDate ?startDate . ?dateRange " \
                   "dcat:endDate ?endDate . BIND (IF(?startParam <= year(?startDate), year(?startDate), ?startParam) " \
                   "AS ?max_start_date) BIND (IF(?endParam <= year(?endDate), ?endParam, year(?endDate)) AS " \
                   "?min_end_date) FILTER (isNumeric(?startParam) && ?max_start_date <= year(?endDate) && isNumeric(" \
                   "?endParam) && ?min_end_date >= year(?startDate)) . } FILTER ((bound(?startDate) || ?startParam " \
                   "='NaN') && (bound(?endDate) || ?endParam = 'NaN')) } "
        interpolator = PlaceholderInterpolator()
        result = interpolator.interpolate(template, label)
        self.assertTrue(result.find(label['<dataset_theme>']) > 0)

    def test_date_interpolator(self):
        template = "select distinct ?dataset ?startDate ?endDate where { ?dataset a agricore:Dataset . ?dataset " \
                   "dcat:theme ?theme . FILTER ( ?theme IN (?subject)) . { SERVICE " \
                   "<http://publications.europa.eu/webapi/rdf/sparql> { SELECT  ?subject WHERE { graph " \
                   "<http://publications.europa.eu/resource/authority/data-theme> { ?subject  ?predicate  ?object . } " \
                   "?object a skos:Concept . ?subject skos:prefLabel ?label . FILTER ( regex(?label, " \
                   "'<dataset_theme>'@en, 'i') ) . } } } BIND (IF(STRLEN(str(<start_date>)) > 0, <start_date>, " \
                   "'NaN') AS ?startParam) BIND (IF(STRLEN(str(<end_date>)) > 0, <end_date>, 'NaN') AS ?endParam) " \
                   "OPTIONAL { ?dataset terms:temporal ?dateRange . ?dateRange a terms:PeriodOfTime . ?dateRange " \
                   "dcat:startDate ?startDate . ?dateRange dcat:endDate ?endDate . BIND (IF(?startParam <= year(" \
                   "?startDate), year(?startDate), ?startParam) AS ?max_start_date) BIND (IF(?endParam <= year(" \
                   "?endDate), ?endParam, year(?endDate)) AS ?min_end_date) FILTER (isNumeric(?startParam) && " \
                   "?max_start_date <= year(?endDate) && isNumeric(?endParam) && ?min_end_date >= year(?startDate)) . " \
                   "} FILTER ((bound(?startDate) || ?startParam ='NaN') && (bound(?endDate) || ?endParam = 'NaN')) }"
        label = {
            '<dataset_theme>': "land cover",
            '<start_date>': "''",
            '<end_date>': "''"
        }
        interpolator = PlaceholderInterpolator()
        result = interpolator.interpolate(template, label)
        self.assertTrue(result.find(label['<start_date>']) > 0)

    def test_get_random_label(self):
        label = {
            '<dataset_theme>': ["land cover", "environment"],
            '<start_date>': ["''", "2000"],
            '<end_date>': ["''", "2009"]
        }
        interpolator = PlaceholderInterpolator()
        result = interpolator.get_random_label(label)
        self.assertIn(result['<dataset_theme>'], label['<dataset_theme>'])
        self.assertIn(result['<start_date>'], label['<start_date>'])
        self.assertIn(result['<end_date>'], label['<end_date>'])


if __name__ == '__main__':
    unittest.main()
