## Prerequisites

- Python 3.7 (or newer)
- docker 20.10 (or newer)
- docker-compose 1.28 (or newer) or docker-compose-plugin

## Module Details

The Agricore Semantic Services are composed by three main modules:

- A GraphDB instance;
- A Sequence to Sequence (Seq2Seq) model;
- An Haystack pipeline.

### GraphDB

A GraphDB instance is used to store Ardit datasets and vocabularies, in order to
perform queries and search for specific datasets based on an input Natural Language
query, which will actually be the user query in the Ardit platform.

To correctly use the service, it is mandatory to run an instance of GraphDB on a remote
server or in a local machine. A Docker image has been created to launch said instance,
already configured to use the default Agricore URI and with the standard Agricore
ontology already upload on it.

As said above, the instance already contains the Agricore ontology, but it does not contain vocabularies,
which have to be inserted separately. The `samples` directory in the repository contains some example
vocabularies that can be uploaded to GraphDB. To add them to the running instance, follow these steps:

1. Open your browser and, in the URL bar, type `http://localhost:8200`;
2. Once the web page is opened, on the left side-bar click on the button _Import_ and then click on _RDF_
   when the menu appears;
3. In the new page, click on _Upload RDF files_;
4. Select the files in the `samples/vocabularies` directory;
5. When redirected to the web page, import each file separately using the button on the
   right side of each vocabulary row;
    - When prompted to the _Import settings_ dialog, make sure to select _named graph_
      and type a custom name for the vocabulary. This is needed to make the default graph
      contain only the Agricore ontology and the datasets.
    - Ensure the file has been correctly uploaded clicking on the _Explore_ button on
      the left side and then clicking on _Graphs overview_; the graph name should appear
      on a new row in the page's main content.

### Seq2Seq model

The main purpose of the model is to transform an NLP query into a SPARQL query, that
can be then given to the GraphDB instance to search for particular datasets on the
DB.

When the GraphDb instance is up and running and filled with the needed Agricore vocabularies,
it can be used to generate a set of labels that will be interpolated to another set
of templates that will compose the Seq2Seq model datasets.  
The templates can be found in the `seq2seq_model/templates` directory. You can read
more about their structure in the `seq2seq_model/dataset_generator` _README_ file.

First thing to do is to create the labels. The _README_ file in the
`seq2seq_model/labels_extraction` folder explains how to do that. The final
result will be a folder containing a list of labels in `.txt` files.

When the labels are extracted, they can be used to actually create the model
input dataset. The _README_ contained in the folder `seq2seq_model/dataset_generator` describes
in details how to perform this process, using both the labels generated in the
previous step and the supplied templates.

The last step is to actually train the Seq2Seq model. A T5 type of model has been chosen
to perform the NLP-SPARQL conversion, given its predisposition to better understand
not ordinary characters like curly brackets (that SPARQL queries often adopt in their
structure). The model can be trained using the Jupyter notebooks provided in the folder
`seq2seq_model/jupyter_notebooks`.  
A Tokenizer-related notebook is also present, which is needed to make the model
recognize all the characters in the templates; therefore this notebook
has to be run before the model notebook.

### Haystack Pipeline

When the model is ready, it can be stored somewhere and used with a custom Haystack
pipeline. There are two main pipelines useful for the semantic services:

- an index pipeline, which lets Ardit upload new datasets and vocabularies to the
  GraphDB instance;
- a search pipeline, which transform a given NL input query into a SPARQL query
  using the model trained in the previous steps and runs that query on the GraphDB
  instance to search for specific filtered datasets, which names are then returned.

To launch pipelines, Haystack expose ReST API that can be used running the custom image created for
Agricore Semantic Services modules.

## Folder Structure

The repository consists of two main folders, `semantic_services` and `seq2seq_model`.    
Refer to the _READMEs_ in each one of these folders to better understand their contents.

## Run

Semantic services module can be deployed using `docker compose` provided in the root folder of the repository.

To use the correct image deployed on gitlab container registry, use the env variable `GITLAB_PROJECT_DIR`.

The `graphdb` container needs 2 volumes:

- `GRAPHDB_HOME`: to hold data loaded (triples) after stop the container
- `IMPORT_FOLDER`: to preload data in to the repository

The `.env` file can be used to set all environment variables.

**Notes**

- The T5 seq2seq model is trained with a limited number of questions and possible values: to retrieve data you have to
  use
  questions that can be understood by the model.
- The T5 seq2seq model currently does not perform well. In order to have a reply in short time from the `/query`
  endpoint
  you have to deploy the GPU version of haystack using the `docker-compose-GPU.yml`.

```shell
docker compose [-f docker-compose-GPU.yml] up 
```

## Test

After run the containers you can use the semantic services module through the ReST API exposed by haystack container.

- `POST /upload`: to invoke the index pipeline that download files and import them into the graphdb repository
- `POST /query`: to search data by natural language question

### `POST /upload`

To upload triples to the default graph of graphdb repository you can use a payload similar to the following one

```json
{
  "format": {
    "mimeType": "text/turtle",
    "extension": "ttl"
  },
  "urls": [
    "https://saref.etsi.org/core/v3.1.1/saref.ttl"
  ]
}
```

To upload triples to a named graph of graphdb repository you can use a payload similar to the following one

```json
{
  "format": {
    "mimeType": "text/turtle",
    "extension": "ttl"
  },
  "urls": [
    "https://saref.etsi.org/core/v3.1.1/saref.ttl"
  ],
  "graph_uri": "htps://saref.etsi.org/core/"
}
```

### `POST /query`

To ask data using natural language question you can use a payload similar to the following one

``json
{
"query": "Find all datasets that cover the theme land cover"
}
``

## License

© Open Source Licence - AGRICORE
