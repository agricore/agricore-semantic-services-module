import os
from os import remove
from os.path import exists
from pathlib import Path

import pytest
import requests

from semantic_services.haystack.pipeline.ardit_crawler import ArditCrawler
from semantic_services.haystack.pipeline.index_pipeline_params import IndexPipelineFormat, IndexPipelineParams
from .conftest import DOWNLOAD_PATH, SAMPLES_PATH


class MockResponse:
    status_code = 200

    @property
    def text(self):
        with open(SAMPLES_PATH / "dataset-types.ttl") as f:
            response = f.read()
        return response

    def raise_for_status(self):
        pass


@pytest.fixture(scope="function")
def ardit_crawler():
    download_path = os.environ["FILE_UPLOAD_PATH"] = DOWNLOAD_PATH
    return ArditCrawler(download_path=download_path)


@pytest.fixture(scope="function")
def mock_get_requests(monkeypatch):
    def mock_get(*args, **kwargs):
        return MockResponse()

    monkeypatch.setattr(requests, "get", mock_get)
    yield
    # delete downloaded file
    if exists(Path(DOWNLOAD_PATH) / "downloaded_file_1.ttl"):
        remove(Path(DOWNLOAD_PATH) / "downloaded_file_1.ttl")


@pytest.mark.usefixtures(ardit_crawler.__name__, mock_get_requests.__name__)
def test_crawler_should_save_file_to_disk(ardit_crawler):
    urls = ["https://www.ardit.com/get_turtle_file"]
    format = IndexPipelineFormat.Turtle
    graph_uri = ""
    params = IndexPipelineParams(
        type=type, urls=urls, format=format,
        graph_uri=graph_uri
    )
    download_file = f'{DOWNLOAD_PATH}/downloaded_file_1.{format.value["extension"]}'
    output_dict, _ = ardit_crawler.run(pipeline_params=params)
    assert download_file in output_dict["paths"], "File non downloaded"
    assert exists(Path(DOWNLOAD_PATH) / "downloaded_file_1.ttl"), "File not exists"
