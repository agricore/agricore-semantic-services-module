import pytest
from haystack.document_stores import GraphDBKnowledgeGraph

from semantic_services.haystack.pipeline.t5_text_2_sparql import T5Text2SparqlRetriever
from .conftest import mock_ardit_pipeline_nodes, PIPELINE_PATH, TEST_QUERY


@pytest.mark.usefixtures(mock_ardit_pipeline_nodes.__name__)
def test_service_replaced(monkeypatch):
    knowledge_graph = GraphDBKnowledgeGraph()
    retriever = T5Text2SparqlRetriever(
        knowledge_graph=knowledge_graph,
        model_name_or_path="model",
        look_up_file=str(PIPELINE_PATH / "lookup_table.json")
    )
    output_query = retriever.substitute_services(TEST_QUERY)
    assert "SPARQL_ENDPOINT" not in output_query
    assert "EU_AUTH_THEME_GRAPH" not in output_query
    assert "EU_INSPIRE_THEME_GRAPH" not in output_query
