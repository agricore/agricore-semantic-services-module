import asyncio
from os import environ
from pathlib import Path
from time import sleep
from typing import Optional, List

import docker
import pytest
import requests
from docker import DockerClient
from docker.models.containers import Container
from dotenv import load_dotenv
from haystack.document_stores import GraphDBKnowledgeGraph
from transformers import T5ForConditionalGeneration, AutoTokenizer
from urllib3.exceptions import ProtocolError

from semantic_services.haystack.pipeline.ext_graphdb_knowledge_graph import ExtGraphDbKnowledgeGraph
from semantic_services.haystack.pipeline.graphdb_store import GraphDBStore

PIPELINE_PATH = Path(__file__).parent.parent / "pipeline"
SAMPLES_PATH = Path(__file__).parent / "samples"
DOWNLOAD_PATH = "/tmp/downloaded_files"

GRAPHDB_HOST = "localhost"
GRAPHDB_PORT = 8200
GRAPHDB_INDEX = "ardit"

TEST_QUERY = """select distinct ?dataset where {
    ?dataset a agricore:Dataset .
    {
        ?dataset dcat:theme ?theme .
        FILTER ( ?theme IN (?subject)) .
        {
            SERVICE SPARQL_ENDPOINT {
                SELECT  ?subject WHERE {
                    graph EU_AUTH_THEME_GRAPH {
                        ?subject  ?predicate  ?object .
                    } ?object a skos:Concept .
                    ?subject skos:prefLabel ?label .
                    FILTER ( regex(?label, 'energy'@en, 'i') ) .
                } 
            } 
        } union {
            select ?subject where {
                graph EU_INSPIRE_THEME_GRAPH {
                    ?subject  ?predicate  ?object .
                } ?object a <http://purl.org/net/provenance/ns#DataItem> .
                ?object <http://purl.org/dc/terms/title> ?title .
                FILTER ( lang(?title) = 'en') FILTER ( regex(?title, 'energy', 'i') ) .
            } 
        } 
    } BIND (IF(STRLEN(str('')) > 0, '', 'NaN') AS ?startParam)
    BIND (IF(STRLEN(str('')) > 0, '', 'NaN') AS ?endParam) OPTIONAL {
        ?dataset terms:temporal ?dateRange .
        ?dateRange a terms:PeriodOfTime .
        ?dateRange dcat:startDate ?startDate .
        ?dateRange dcat:endDate ?endDate .
        BIND (IF(?startParam <= year(?startDate), year(?startDate), ?startParam) AS ?max_start_date)
        BIND (IF(?endParam <= year(?endDate), ?endParam, year(?endDate)) AS ?min_end_date)
        FILTER (isNumeric(?startParam) && ?max_start_date <= year(?endDate) && isNumeric(?endParam)
        && ?min_end_date >= year(?startDate)) .
    } FILTER ((bound(?startDate) || ?startParam ='NaN') && (bound(?endDate) || ?endParam = 'NaN')) 
}"""

graphdb_store: GraphDBStore = None
graphdb_wrapper: ExtGraphDbKnowledgeGraph = None


class MockKG(GraphDBKnowledgeGraph):
    def upload_triples(self, *a, **k):
        print("******** upload triples")

    def import_from_ttl_file(self, *a, **k):
        pass

    def query(self, *a, **k):
        return [
            {
                "0": {
                    "value": "Lucas 2009"
                },
                "1": {
                    "value": "Lucas 2018"
                }
            }
        ]


class MockModel:
    def generate(self, inputs, **args) -> List[str]:
        return [
            "encoded sparql query"
        ]


class MockTokenizer:
    def __call__(self, query_list: list, **kwargs) -> dict:
        return {
            "input_ids": ""
        }

    def decode(self, input_value, **args) -> str:
        return TEST_QUERY


@pytest.fixture(scope="function")
def mock_ardit_pipeline_nodes(monkeypatch):
    mock_kg = MockKG()
    monkeypatch.setattr(ExtGraphDbKnowledgeGraph,
                        "upload_triples",
                        lambda *a, **k: mock_kg.upload_triples(*a, **k))
    monkeypatch.setattr(ExtGraphDbKnowledgeGraph,
                        "query",
                        lambda *a, **k: mock_kg.query(*a, **k))
    monkeypatch.setattr(T5ForConditionalGeneration,
                        "from_pretrained",
                        lambda x: MockModel())
    monkeypatch.setattr(AutoTokenizer,
                        "from_pretrained",
                        lambda x: MockTokenizer())


@pytest.fixture(scope="session")
def run_graphdb_docker():
    load_dotenv()
    registry = "registry.gitlab.com"
    username = environ.get("CI_REGISTRY_USER") if environ.get("CI_REGISTRY_USER") else environ.get("GITLAB_USERNAME")
    project_path = environ.get("GITLAB_PROJECT_PATH")
    print(f"The project path env is {project_path}")
    graphdb_container_name = f"{registry}/{project_path}/graphdb:9.10.3-free"
    token = environ.get('CI_BUILD_TOKEN') if environ.get('CI_BUILD_TOKEN') else "VTz9rvhBkYxDBRwVA1UF"

    client: DockerClient
    client = docker.from_env()
    client.login(username=username, password=token, registry=registry)
    print("Starting the GraphDB Container")
    graphdb_container: Container = client.containers.run(
        image=graphdb_container_name,
        detach=True,
        ports={f"{GRAPHDB_PORT}": GRAPHDB_PORT}
    )
    asyncio.run(wait_for_running())
    yield
    graphdb_container.stop()
    graphdb_container.remove()


async def wait_for_running():
    """
    Check each 3 seconds if the GraphDB instance is running through a simple
    HTTP request. When the request returns 200, the instance is ready to
    accept updates on its repository.
    """
    timeout = 120
    stop_time = 3
    elapsed_time = 0
    status = get_active_location_status_code() == 200
    while not status and elapsed_time < timeout:
        print(f'Trying connecting to GraphDB, elapsed time: {elapsed_time}s')
        status = get_active_location_status_code() == 200
        if status:
            print("Started container correctly")
        else:
            sleep(stop_time)
            elapsed_time += stop_time
    if elapsed_time >= timeout:
        raise Exception("Timeout trying to connect to the GraphDB instance")


# -- Utils --

def get_active_location_status_code():
    try:
        url = "http://localhost:8200/rest/locations/active"
        response = requests.get(url=url, timeout=30)
        status_code = response.status_code
    except (requests.exceptions.ConnectionError, ProtocolError):
        status_code = 500
    return status_code


def get_repository_contexts(index: Optional[str]):
    url = f"http://localhost:8200/repositories/{index if index else 'ardit'}/contexts"
    headers = {
        "Accept": "application/json"
    }
    response = requests.get(url=url, headers=headers, timeout=20)
    results = response.json()
    return list(map(lambda result: result["contextID"]["value"], results["results"]["bindings"]))


def get_repository_size(index: Optional[str]):
    url = f"http://localhost:8200/rest/repositories/{index if index else 'ardit'}/size"
    headers = {
        "Accept": "application/json"
    }
    response = requests.get(url=url, headers=headers, timeout=20)
    sizes = response.json()
    return sizes["total"]
