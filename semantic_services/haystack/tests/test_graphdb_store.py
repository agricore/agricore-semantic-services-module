from time import sleep

import pytest

from semantic_services.haystack.pipeline.ext_graphdb_knowledge_graph import ExtGraphDbKnowledgeGraph
from semantic_services.haystack.pipeline.graphdb_store import GraphDBStore
from semantic_services.haystack.pipeline.index_pipeline_params import IndexPipelineFormat, IndexPipelineParams
from .conftest import run_graphdb_docker, get_repository_size, \
    get_repository_contexts, SAMPLES_PATH, GRAPHDB_HOST, GRAPHDB_PORT, GRAPHDB_INDEX


@pytest.fixture
def graphdb_wrapper():
    graphdb_wrapper = ExtGraphDbKnowledgeGraph(
        host=GRAPHDB_HOST,
        port=GRAPHDB_PORT,
        index=GRAPHDB_INDEX
    )
    return graphdb_wrapper


@pytest.fixture
@pytest.mark.usefixtures(graphdb_wrapper.__name__)
def graphdb_store(graphdb_wrapper):
    return GraphDBStore(knowledge_graph=graphdb_wrapper)


@pytest.mark.integration
@pytest.mark.usefixtures(run_graphdb_docker.__name__, graphdb_wrapper.__name__, graphdb_store.__name__)
def test_should_update_default_graph(graphdb_wrapper, graphdb_store):
    urls = [""]
    pl_format = IndexPipelineFormat.Turtle
    download_path = ""
    params = IndexPipelineParams(format=pl_format, urls=urls,
                                 download_path=download_path, graph_uri=None)
    paths = [
        SAMPLES_PATH / "dataset-types.ttl"
    ]
    before_update_size = get_repository_size(index=None)
    graphdb_store.run(pipeline_params=params, paths=paths)
    # Sleep 1 second to make the GraphDB instance update itself
    sleep(1)
    after_update_size = get_repository_size(index=None)
    assert after_update_size > before_update_size, "Graph correcly updated"


@pytest.mark.integration
@pytest.mark.usefixtures(run_graphdb_docker.__name__, graphdb_wrapper.__name__, graphdb_store.__name__)
def test_should_update_custom_graph(graphdb_wrapper, graphdb_store):
    urls = [""]
    pl_format = IndexPipelineFormat.Turtle
    download_path = ""
    graph_uri = "https://agricore-project.eu/ontology/agricore-dcatap/subjects"
    params = IndexPipelineParams(format=pl_format, urls=urls,
                                 download_path=download_path, graph_uri=graph_uri)
    paths = [
        SAMPLES_PATH / "dataset-types.ttl"
    ]
    query = """
    select ?s ?p ?o 
    where {
        graph <https://agricore-project.eu/ontology/agricore-dcatap/subjects> {
            ?s ?p ?o .
        }
    }
    """
    # compute how many triples are in the named graph "graph_uri" and check last uploaded triple
    graph_triples_before_upload = graphdb_wrapper.query(sparql_query=query)
    graph_size = len(graph_triples_before_upload)
    triple_not_present = next((i for i, item in enumerate(graph_triples_before_upload) if
                               (item['s'][
                                    'value'] == "https://agricore-project.eu/ontology/agricore-dcatap/subjects") and
                               item['p']['value'] == "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" and
                               item['o']['value'] == "http://www.w3.org/2004/02/skos/core#ConceptScheme"), None)
    assert triple_not_present is None, "Some triples are present"

    graphdb_store.run(pipeline_params=params, paths=paths)
    # Sleep 1 second to make the GraphDB instance update itself
    sleep(3)
    contexts = get_repository_contexts(index=None)
    assert graph_uri in contexts, "Graph URI is in the contexts"
    # check that some triples present in the turtle file "dataset-types.ttl" have been successfully uploaded
    graph_triples_after_upload = graphdb_wrapper.query(sparql_query=query)
    graph_size_after_upload = len(graph_triples_after_upload)
    assert graph_size_after_upload > graph_size, "Graph correcly updated"
    triple_present_after_upload = next((i for i, item in enumerate(graph_triples_after_upload) if
                                        (item['s'][
                                             'value'] == "https://agricore-project.eu/ontology/agricore-dcatap/subjects") and
                                        item['p']['value'] == "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" and
                                        item['o']['value'] == "http://www.w3.org/2004/02/skos/core#ConceptScheme"),
                                       None)
    assert triple_present_after_upload is not None, "Triples are stored in the main graph"
    assert triple_present_after_upload > 0
