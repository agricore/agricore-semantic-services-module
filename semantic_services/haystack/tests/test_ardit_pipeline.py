import pytest

from .conftest import PIPELINE_PATH, mock_ardit_pipeline_nodes
from semantic_services.haystack.pipeline.index_pipeline_params import IndexPipelineParams, IndexPipelineFormat

from haystack import Pipeline


@pytest.mark.pipeline
@pytest.mark.usefixtures(mock_ardit_pipeline_nodes.__name__)
def test_load_ardit_indexing_pipeline():
    index_pipeline_params: IndexPipelineParams = IndexPipelineParams()
    index_pipeline_params.format = IndexPipelineFormat.Turtle
    index_pipeline_params.urls = ["https://saref.etsi.org/core/v3.1.1/saref.ttl"]
    params = {
        "pipeline_params": index_pipeline_params
    }
    config_path = PIPELINE_PATH / "agricore-pipeline.yml"
    indexing_pipeline = Pipeline.load_from_yaml(path=config_path, pipeline_name="ardit-indexing")
    assert indexing_pipeline is not None, "Error loading index pipeline"
    result = indexing_pipeline.run(params=params)
    assert 'paths' in result
    assert '/tmp/downloaded_file_1.ttl' in result['paths']


@pytest.mark.pipeline
@pytest.mark.usefixtures(mock_ardit_pipeline_nodes.__name__)
def test_load_ardit_search_pipeline():
    config_path = PIPELINE_PATH / "agricore-pipeline.yml"
    query = "Find all datasets with theme energy"
    search_pipeline = Pipeline.load_from_yaml(path=config_path, pipeline_name="ardit-search")
    assert search_pipeline is not None, "Error loading search pipeline"
    result = search_pipeline.run(query=query, params={})
    assert "answers" in result
    assert len(result["answers"])
    assert "answer" in result["answers"][0]
    assert len(result["answers"][0]["answer"])
    assert result["answers"][0]["answer"] == "Lucas 2009"
