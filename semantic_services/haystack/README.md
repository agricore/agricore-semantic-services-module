## Haystack Extension

### Docker Instructions

To build the Docker image:

```shell
docker build -t test-haystack .
```

To run the container:

```shell
docker run -d --rm --name myhaystack -p 8000:8000 test-haystack
```