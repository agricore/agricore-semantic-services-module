import os
from pathlib import Path
from typing import Tuple, Dict, Type, Optional
import logging
import requests
from haystack.nodes.base import BaseComponent

from ..pipeline.index_pipeline_params import IndexPipelineParams, IndexPipelineFormat

logger = logging.getLogger('haystack.custom_components.ardit_crawler')

class ArditCrawler(BaseComponent):
    """
    Downloads semantic-related files from a given URL and saves them into a given directory.
    Returns the paths where the files have been saved.
    The `run` method params have to be of type IndexPipelineParams

    **Example:**
    ```
    |    from haystack.nodes.crawler import ArditCrawler
    |
    |    urls = ["https://www.ardit.com/get_turtle_file"]
    |    format = IndexPipelineFormat.Turtle
    |    type = IndexPipelineType.Dataset
    |    download_path = "path_to_store_files"
    |    graph_uri = "my_uri"
    |    params = IndexPipelineParams(urls=urls, format=format,
    |        type=type, download_path=download_path
    |        graph_uri=graph_uri)
    |
    |    crawler = ArditCrawler()
    |    output = crawler.run(params=params)
    ```

    """

    outgoing_edges = 1
    download_path = None

    def __init__(self, download_path: Optional[str] = None):
        if not download_path:
            self.download_path = os.getenv("FILE_UPLOAD_PATH", str((Path(__file__).parent / "ardit-downloads").absolute()))
        else:
            self.download_path = download_path
        os.makedirs(self.download_path, exist_ok=True)


    def run(self, pipeline_params: IndexPipelineParams) -> Tuple[Dict, str]:
        paths = []
        for index, url in enumerate(pipeline_params.urls):
            response = self.request_file(url, pipeline_params.format)
            filepath = Path(self.download_path + '/downloaded_file_' + str(index + 1) + '.' + pipeline_params.format.value["extension"])
            logger.info("download path file %s", filepath)
            with filepath.open("w", encoding='UTF-8') as f:
                f.write(response.text)
            paths.append(filepath.as_posix())
        output_dict = {
            "paths": paths
        }
        logger.info("saved files %s", output_dict)
        return output_dict, "output_1"

    def request_file(self, url: str, allowed_format=Type[IndexPipelineFormat]):
        """
        Makes a request to a given URL, accepting a particular type format in response
        """
        headers = {"Accept": allowed_format.value["mimeType"]}
        response = requests.get(url=url, headers=headers)
        response.raise_for_status()
        return response

    def run_batch(self):
        pass
