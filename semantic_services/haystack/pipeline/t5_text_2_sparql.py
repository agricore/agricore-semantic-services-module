import json
import logging
from pathlib import Path
from typing import Optional, Dict

from haystack.nodes.retriever.base import BaseGraphRetriever
from transformers import T5ForConditionalGeneration, AutoTokenizer

logger = logging.getLogger('haystack.custom_components.t5_text_2_sparql')


class T5Text2SparqlRetriever(BaseGraphRetriever):
    """
    Graph retriever that uses a pre-trained Bart model to translate natural language questions
    given in text form to queries in SPARQL format.
    The generated SPARQL query is executed on a knowledge graph.
    """

    look_up_table: Dict[str, str] = None
    knowledge_graph = None

    def __init__(self, knowledge_graph,
                 model_name_or_path: str, look_up_file: str,
                 top_k: int = 1, use_gpu: bool = False):
        """
        Init the Retriever by providing a knowledge graph and a pre-trained BART model

        :param knowledge_graph: An instance of BaseKnowledgeGraph on which to execute SPARQL queries.
        :param model_name_or_path: Name of or path to a pre-trained BartForConditionalGeneration model.
        :param look_up_file: Path of the JSON file containing the mapping between
        service placeholder and service URI
        :param top_k: How many SPARQL queries to generate per text query.
        """

        super().__init__()

        self.knowledge_graph = knowledge_graph
        self.use_gpu = use_gpu
        if use_gpu:
            self.model = T5ForConditionalGeneration.from_pretrained(model_name_or_path).to("cuda")
        else:
            self.model = T5ForConditionalGeneration.from_pretrained(model_name_or_path)
        self.tok = AutoTokenizer.from_pretrained(model_name_or_path)
        self.top_k = top_k
        # Load the look-up file for services replacement process
        try:
            with open(Path(__file__).parent / look_up_file, "r") as f:
                self.look_up_table = json.loads(f.read())
        except Exception as e:
            logger.error("There was an error loading the services look up JSON file")
            raise e
        logger.info("Initialized T5Text2SparqlRetriever instance")

    def retrieve(self, query: str, top_k: Optional[int]):
        """
        Translate a text query to SPARQL and execute it on the knowledge graph to retrieve a list of answers

        :param query: Text query that shall be translated to SPARQL and then executed on the knowledge graph
        :param top_k: How many SPARQL queries to generate per text query.
        """
        logger.info("Started retrieve method from T5Text2SparqlRetriever")
        if top_k is None:
            top_k = self.top_k
        if self.use_gpu:
            inputs = self.tok([query], max_length=300, truncation=True, return_tensors="pt").to("cuda")
        else:
            inputs = self.tok([query], max_length=300, truncation=True, return_tensors="pt")
        logger.info("Encoded the input query with the tokenizer")
        generated_value = self.model.generate(
            inputs["input_ids"], num_beams=20, max_length=5000, num_return_sequences=top_k, early_stopping=True
        )
        logger.info(f"Generated sparql queries (encoded)")
        sparql_queries = [
            self.tok.decode(g, skip_special_tokens=True, clean_up_tokenization_spaces=False) for g in generated_value
        ]
        logger.info(f"Decoded the queries in SPARQL language")
        answers = []
        #
        # answers: [{answer: ""}, {answer: ""}]
        #
        for sparql_query in sparql_queries:
            correct_query = self.substitute_services(sparql_query)
            logger.info("Executing query: %s", correct_query)
            ans, query = self._query_kg(sparql_query=correct_query)
            logger.info("results: %s %s", ans, len(ans))
            for a in ans:
                answers.append((a, query))

        logger.info("answer: %s", answers)
        # if there are no answers we still want to return something
        if len(answers) == 0:
            logger.error("ERROR: there were no answers returned")
            answers.append(("", ""))
        results = answers[: top_k]
        results = [self.format_result(result) for result in results]
        logger.info("Retrieve method completed")
        logger.info("results: %s", results)
        return results

    def _query_kg(self, sparql_query):
        """
        Execute a single SPARQL query on the knowledge graph to retrieve an answer and unpack
        different answer styles for boolean queries, count queries, and list queries.

        :param sparql_query: SPARQL query that shall be executed on the knowledge graph
        """
        try:
            response = self.knowledge_graph.query(sparql_query=sparql_query)

            # unpack different answer styles
            if isinstance(response, list):
                if len(response) == 0:
                    result = ""
                else:
                    result = []
                    for x in response:
                        for k, v in x.items():
                            result.append(v["value"])
            elif isinstance(response, bool):
                result = str(response)
            elif "count" in response[0]:
                result = str(int(response[0]["count"]["value"]))
            else:
                result = ""

        except Exception:
            result = ""

        return result, sparql_query

    def substitute_services(self, query: str) -> str:
        """
        Uses the look up table to substitute each service placeholder in the input query string
        with the correct service URI, to generate the right query.
        """
        for k, v in self.look_up_table.items():
            query = query.replace(k, v)
        return query

    def format_result(self, result):
        """
        Generate formatted dictionary output with text answer and additional info

        :param result: The result of a SPARQL query as retrieved from the knowledge graph
        """
        query = result[1]
        prediction = result[0]
        prediction_meta = {"model": self.__class__.__name__, "sparql_query": query}

        return {"answer": prediction, "prediction_meta": prediction_meta}

    def eval(self):
        raise NotImplementedError

    def run(self, query, *args, **kwargs):
        return super().run(query, *args, **kwargs)

    def run_batch(self, *args, **kwargs):
        return super().run_batch(*args, **kwargs)

    def retrieve_batch(self, *args, **kwargs):
        return super().retrieve_batch(*args, **kwargs)
