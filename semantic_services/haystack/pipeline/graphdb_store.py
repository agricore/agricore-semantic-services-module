from pathlib import Path
from typing import Tuple, Dict, List
import logging
from haystack import BaseComponent

from ..pipeline.ext_graphdb_knowledge_graph import ExtGraphDbKnowledgeGraph
from ..pipeline.index_pipeline_params import IndexPipelineParams

logger = logging.getLogger('haystack.custom_components.graphdb_store')


class GraphDBStore(BaseComponent):
    """
    Uses the ExtGraphDbKnowledgeGraph class to upload the triples into an already-running
    GraphDB instance.
    """

    outgoing_edges = 1
    knowledge_graph: ExtGraphDbKnowledgeGraph

    def __init__(self, knowledge_graph: ExtGraphDbKnowledgeGraph):
        self.knowledge_graph = knowledge_graph

    def run(self, pipeline_params: IndexPipelineParams, paths: List[str]) -> Tuple[Dict, str]:
        logger.info("pipeline params %s", pipeline_params)
        logger.info("file paths %s", paths)
        for path in paths:
            self.knowledge_graph.upload_triples(
                index=None,
                graph_name=pipeline_params.graph_uri,
                path=Path(path),
                file_mimetype=pipeline_params.format.value["mimeType"]
            )
        output_obj = {}
        return output_obj, "output_1"

    def run_batch(self):
        pass
