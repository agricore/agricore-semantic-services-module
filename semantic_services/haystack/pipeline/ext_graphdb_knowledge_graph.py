import json
import logging
import time
from pathlib import Path
from typing import Optional, Dict

import requests
from haystack.document_stores import GraphDBKnowledgeGraph
from requests.auth import HTTPBasicAuth

logger = logging.getLogger('haystack.custom_components.ext_graphdb_knowledge_graph')


class ExtGraphDbKnowledgeGraph(GraphDBKnowledgeGraph):
    """
    Knowledge graph extension of Haystack original GrapbDBKnowledgeGraph class. 
    
    In the present class we implement a new method that can upload a new set of triples in a named graph
    different from the default one of the GraphDB instance. 
    """

    def __init__(
            self,
            host: str = "localhost",
            port: int = 7200,
            username: str = "",
            password: str = "",
            index: Optional[str] = None,
            prefixes: str = "",
    ):
        """
        Init the knowledge graph by defining the settings to connect with a GraphDB instance;
        all params are passed to the super class in order to set up the connection with the database.

        :param host: address of server where the GraphDB instance is running
        :param port: port where the GraphDB instance is running
        :param username: username to login to the GraphDB instance (if any)
        :param password: password to login to the GraphDB instance (if any)
        :param index: name of the index (also called repository) stored in the GraphDB instance
        :param prefixes: definitions of namespaces with a new line after each namespace, e.g., 
            PREFIX agricore: <https://agricore-project.eu/ontology/agricore-dcatap#>
        """
        # TODO - move to env variable or external file
        self.prefixes = """PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            PREFIX agricore: <https://agricore-project.eu/ontology/agricore-dcatap#>
            PREFIX dcat: <http://www.w3.org/ns/dcat#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX terms: <http://purl.org/dc/terms/>
            PREFIX prov: <http://purl.org/net/provenance/ns#> 
        
        """
        super().__init__(
            host=host,
            port=port,
            username=username,
            password=password,
            index=index,
            prefixes=self.prefixes
        )

    def upload_triples(self,
                       index: Optional[str],
                       graph_name: Optional[str],
                       path: Path,
                       file_mimetype: str = 'ttl',
                       headers: Optional[Dict[str, str]] = None,
                       replace_data: bool = False) -> None:
        """
        Read the file specified at the given path and upload the new set of triples onto the graph identified by its name 
        of the specified index of GraphDB. 

        :param index: name of the repository of the GraphDB instance
        :param graph_name: name of the graph where to upload the set of new triples
        :param path: path of the file containing the set of new triples
        :param file_mimetype: mimetype of the data to be uploaded via GraphDB ReST API 
        :param headers: optional headers to include in the GraphDB upload ReST API 
        :param replace_data: flag indicating if existing data on the named graph should be replaced by the new ones
        """
        current_index = index if index else self.index
        logger.info(f"Uploading triples to: {current_index}")
        url = f"{self.url}/rest/data/import/upload/{current_index}/file"
        context = graph_name if graph_name else "default"
        payload = {
            "name": path.name,
            "type": "file",
            "status": "NONE",
            "context": context,
            "replaceGraphs": [graph_name] if replace_data else [],
            "baseURI": graph_name,
            "forceSerial": "false",
            "data": "8bc32c30-5b16-4714-857d-544e271b4dcd",
            "timestamp": round(time.time() * 1000),
            "parserSettings": {"preserveBNodeIds": "false", "failOnUnknownDataTypes": "false",
                               "verifyDataTypeValues": "false", "normalizeDataTypeValues": "false",
                               "failOnUnknownLanguageTags": "false", "verifyLanguageTags": "true",
                               "normalizeLanguageTags": "false", "stopOnError": "true"}
        }

        logger.info(f"Reading the triples in the file: {path} and uploading them on GraphDB...")
        f = open(path, "r", encoding="utf-8")

        files = {"file": (path.name, f, file_mimetype),
                 "importSettings": (None, json.dumps(payload), 'application/json')}
        response = requests.post(
            url,
            files=files,
            headers=headers,
            auth=HTTPBasicAuth(self.username, self.password)
        )
        f.close()
        response.raise_for_status()
