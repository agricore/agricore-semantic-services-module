import enum
from typing import Optional, List
from pydantic import BaseModel


class IndexPipelineFormat(enum.Enum):
    Turtle = {
        "mimeType": "text/turtle",
        "extension": "ttl"
    }
    RDF = {
        "mimeType": "application/rdf+xml",
        "extension": "rdf"
    }
    JsonLD = {
        "mimeType": "application/ld+json",
        "extension": "jsonld"
    }


class IndexPipelineParams(BaseModel):
    urls: List[str] = None
    format: IndexPipelineFormat = None
    graph_uri: Optional[str] = None

    # def __init__(self, type: IndexPipelineType, urls: List[str],
    #              format: IndexPipelineFormat,
    #              graph_uri: Optional[str],
    #              ):
    #     self.type = type
    #     self.urls = urls
    #     self.format = format
    #     self.graph_uri = graph_uri
