import logging
from fastapi import APIRouter, FastAPI, HTTPException
from rest_api.pipeline.index_pipeline_params import IndexPipelineParams
from rest_api.utils import get_app, get_pipelines
from haystack import Pipeline
from rest_api.config import LOG_LEVEL

logging.getLogger("haystack").setLevel(LOG_LEVEL)
logger = logging.getLogger("haystack")

router = APIRouter()
app: FastAPI = get_app()
ardit_indexing_pipeline: Pipeline = get_pipelines().get("indexing_pipeline", None)


@router.post("/upload")
def upload(request: IndexPipelineParams):
    logger.info("Uploading through endpoint /upload")
    if not ardit_indexing_pipeline:
        logger.error('index pipeline %s', ardit_indexing_pipeline)
        raise HTTPException(status_code=501, detail="Indexing Pipeline is not configured.")
    try:
        ardit_indexing_pipeline.run(
            params={"pipeline_params": request}
        )
    except Exception as e:
        logger.exception("Upload error")
        logger.exception(e)
        raise HTTPException(status_code=500, detail="Error on run index pipeline")
