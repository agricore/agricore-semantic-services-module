## Semantic Services

The Agricore semantic services rely on the [Haystack framework](https://haystack.deepset.ai/)
to make available a set of systems useful to interact with a
[GraphDB](https://graphdb.ontotext.com/) instance, which manages the main ontology, the datasets
and the vocabularies.

Through Haystack, it is possible to define pipelines that can be interacted with
via an automatically exposed ReST API.

### GraphDB folder

The GraphDB folder contains:
- a _Dockerfile_ useful to create a Docker image containing a GraphDB instance with
the Agricore ontology already uploaded into it. The image exposes said instance
through the port `8200`;
- the `agricore.ttl` file, which is the main Agricore project ontology;
- the `ardit-repo-config.ttl` file, used by GraphDB to set up a repository with a
custom URI for the default graph and to also import the ontology into the newly
created repository.

### Haystack folder

The Haystack folder contains
- the components used in the two developed pipelines:
- the `YAML` file describing the pipelines' flows and the components each one of them
have to handle;
- different tests files in Python language that ensure the components correct behaviour.
